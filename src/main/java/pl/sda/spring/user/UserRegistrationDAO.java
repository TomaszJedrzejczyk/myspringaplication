package pl.sda.spring.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRegistrationDAO {

    private UserRepository userRepository;

    @Autowired
    public UserRegistrationDAO(UserRepository userRepository) { //to jest w skrocie konstruktor, dependency injection
        this.userRepository = userRepository;
    }

    public void saveNewUser(User user) {
        if (userExists(user.getEmail())) {
            throw new UserExistException("Użytkownik " + user.getEmail() + "już istnieje w bazie");
        }
        userRepository.save(user);
    }

    public boolean userExists(String email) {
        if (userRepository.findUserByEmail(email) != null) {
            return true;
        }
        return false;
    }

}
