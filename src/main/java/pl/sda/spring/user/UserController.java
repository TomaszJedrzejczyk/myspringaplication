package pl.sda.spring.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.sda.spring.Countries;

import javax.validation.Valid;

@Controller
public class UserController {

    private UserRegistrationService userRegistrationService;
    private UserLoginService userLoginService;



    @Autowired
    public UserController(UserRegistrationService userRegistrationService, UserLoginService userLoginService) {
        this.userRegistrationService = userRegistrationService;
        this.userLoginService = userLoginService;
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@ModelAttribute @Valid UserRegistrationDTO userRegistrationDTO, BindingResult validationResult, Model model){ //valid bedzie sprawdzal co przyjdzie w modelu, i prawdzi na postawie tego co mamy w userregistrationdto
        if (validationResult.hasErrors()) {
            return "registerForm";
        }
        userRegistrationService.registerUser(userRegistrationDTO);
        model.addAttribute("userEmail", userRegistrationDTO.getEmail());
        return "registerResult";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register(Model model){
        model.addAttribute("userRegistrationDTO", new UserRegistrationDTO());
        model.addAttribute("countries", Countries.values());
        return "registerForm";
    }

    @GetMapping(value = "/login")
    public String loginForm(){
        return "loginForm";
    }

}
