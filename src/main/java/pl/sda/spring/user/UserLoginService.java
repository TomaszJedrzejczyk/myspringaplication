package pl.sda.spring.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserLoginService {


    private final UserLoginDAO userLoginDAO; // to jest podstawoy field injection

    @Autowired
    public UserLoginService (UserLoginDAO userLoginDAO){
        this.userLoginDAO = userLoginDAO;
    }


}
