package pl.sda.spring.user;

import com.google.common.collect.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRegistrationService {

    private UserRegistrationDAO userRegistrationDAO;
    private UserRegistrationDTOToUserBuilder userRegistrationDTOToUserBuilder;
    private RoleRepository roleRepository;

    @Autowired
    public UserRegistrationService(RoleRepository roleRepository, UserRegistrationDAO userRegistrationDAO, UserRegistrationDTOToUserBuilder userRegistrationDTOToUserBuilder) {
        this.roleRepository = roleRepository;
        this.userRegistrationDAO = userRegistrationDAO;
        this.userRegistrationDTOToUserBuilder = userRegistrationDTOToUserBuilder;
    }

    public void registerUser(UserRegistrationDTO userRegistrationDTO) {
        User user = userRegistrationDTOToUserBuilder.rewriteDTOToUser(userRegistrationDTO);
        Role userRole = roleRepository.findRoleByRoleName("USER");

        if (userRole == null) {
            userRole = new Role("USER");
            roleRepository.save(userRole);
        }
        user.setRoles(Sets.newHashSet(userRole));
        userRegistrationDAO.saveNewUser(user);
    }
}
