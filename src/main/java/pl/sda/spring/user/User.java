package pl.sda.spring.user;

import lombok.Getter;
import lombok.Setter;
import pl.sda.spring.BaseEntity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Setter
@Getter
public class User extends BaseEntity {


    private String firstName;
    private String lastName;
    @Embedded //zawierajace sie w beda uzupelniane na tym samym poziome co reszta bazy
    private UserAddress userAddress;
    private String birthDate;
    private String pesel;
    private String email;
    private String password;
    private String phone;
    private boolean preferEmails;
    @ManyToMany
    @JoinTable(name = "user_role")
    private Set<Role> roles;
}
