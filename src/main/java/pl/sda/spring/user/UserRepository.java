package pl.sda.spring.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {
    User findUserByEmail(String email); //fixme
    //to jest pole bezstanowe poniewaz nie ma zadnych pol a jedynie service
    
    
    
}
