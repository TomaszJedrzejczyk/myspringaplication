package pl.sda.spring;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.jws.WebParam;

@ControllerAdvice
public class ErrorPageController {

    @ExceptionHandler(value = Exception.class)  //ta metoda bedzie reagowana na wszystkie rodzaje exceptionow jakie poleca
    public String exceptionHandler(Exception e, Model model){
        model.addAttribute("exception",e.getMessage());
        return "errorPage";
    }
}
