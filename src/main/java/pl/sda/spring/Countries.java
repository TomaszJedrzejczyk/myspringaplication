package pl.sda.spring;

import lombok.Getter;

import java.util.Arrays;

@Getter
public enum Countries {
    POLAND("PL", "Polska"),
    GERMANY("GR", "Niemcy"),
    RUSSIA("RU", "Rosja"),
    JAPAN("JP", "Japonia"),
    PORTUGAL("PR", "Portugalia"),
    USA("US", "Stany Zjędnoczone"),
    SPAIN("ES", "Hiszpania"),
    ENGLAND("UK", "Wielka Brytania");

    private final String symbol;
    private final String plName;

    Countries(String symbol, String plName) {
        this.symbol = symbol;
        this.plName = plName;
    }

    public static Countries findcCountryBySymbol(String symbol) {
        return Arrays.stream(Countries.values())
                .filter(s -> s.getSymbol().equals(symbol))
                .findFirst().orElse(null);
    }
}
