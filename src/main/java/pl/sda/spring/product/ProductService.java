package pl.sda.spring.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final ProductDAO productDAO;
    private final ProductToProductDTOBuilder productToProductDTOBuilder;

    @Autowired
    public ProductService(ProductDAO productDAO, ProductToProductDTOBuilder productToProductDTOBuilder) {
        this.productDAO = productDAO;
        this.productToProductDTOBuilder = productToProductDTOBuilder;
    }

    public Product createNewProduct(String productName, Integer stockAmount, BigDecimal price) {
        Product product = new Product();
        product.setProductName(productName);
        product.setStockAmount(stockAmount);
        product.setPrice(price);
        return productDAO.saveProduct(product);
    }

    public List<ProductDTO> findsProducts() {
        return productDAO.findProducts().stream().map(p -> productToProductDTOBuilder.rewriteProductToProductDTO(p))
                .collect(Collectors.toList());
    }


    public Optional<Product> findProductById(Integer id) {
        return productDAO.finProductById(id);
    }

    public void updateProduct(Product product) { // todo zmienic product na DTO
        Product productById = productDAO.finProductById(product.getId()).get();
        productById.setProductName(product.getProductName());
        productById.setStockAmount(product.getStockAmount());
        productById.setPrice(product.getPrice());
        productDAO.saveProduct(productById);
    }
}
