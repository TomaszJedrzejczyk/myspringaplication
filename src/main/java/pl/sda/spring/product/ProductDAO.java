package pl.sda.spring.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductDAO {

    @Autowired
    private ProductRepository productRepository;

    public Product saveProduct(Product product) {
        return productRepository.save(product);
    }

    public List<Product> findProducts() { //todo DTO
        return productRepository.findAll();

    }

    public Optional<Product> finProductById(Integer id) {
        return productRepository.findById(id);
    }

    public void updateProduct(Product product) {
    }
}
