package pl.sda.spring.product;

public interface ProductInfoHolder {
    void setId(Integer id);

    void setProductName(String productName);

    void setStockAmount(Integer stockAmount);

    void setPrice(java.math.BigDecimal price);

    Integer getId();

    String getProductName();

    Integer getStockAmount();

    java.math.BigDecimal getPrice();
}
