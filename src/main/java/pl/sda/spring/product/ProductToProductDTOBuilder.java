package pl.sda.spring.product;

import org.springframework.stereotype.Component;

@Component
public class ProductToProductDTOBuilder {

    public ProductDTO rewriteProductToProductDTO(Product product) {
        ProductDTO productDTO = new ProductDTO();
        return (ProductDTO) rewriteFields(product, productDTO);
    }

    public Product rewriteProductDToToProduct(ProductDTO productDTO) {
        Product product = new Product();
        return (Product) rewriteFields(productDTO, product);
    }

    private ProductInfoHolder rewriteFields(ProductInfoHolder source, ProductInfoHolder target) {
        target.setId(source.getId());
        target.setProductName(source.getProductName());
        target.setStockAmount(source.getStockAmount());
        target.setPrice(source.getPrice());
        return target;
    }
}
