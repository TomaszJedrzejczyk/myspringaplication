package pl.sda.spring.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class ProductsContraoller {

    @Autowired
    private ProductService productService;

    @PostMapping(value = "/products")
    public String showProduct(Model model) {
        model.addAttribute("productsList", productService.findsProducts());
        return "products";
    }


}
