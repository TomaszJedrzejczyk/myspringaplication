package pl.sda.spring.product;

import lombok.Getter;
import lombok.Setter;
import pl.sda.spring.BaseEntity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Getter
@Setter
@Entity
public class Product extends BaseEntity implements ProductInfoHolder{


    private String productName;
    private Integer stockAmount;
    private BigDecimal price;
}
